package cat.escolapia.pmdm.android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tonirosa.exemplesClick.R;

public class ClickExampleActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.click_example_activity);
        Toast.makeText(this, "Attachant els evetns als btons i contrls en general", Toast.LENGTH_LONG).show();
        initEvents();


    }

    protected void initEvents() {

        Button btn1 = (Button) findViewById(R.id.btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Codi btn1
                Toast.makeText(getApplicationContext(), "Exemple click btn1", Toast.LENGTH_LONG).show();
            }
        });

        Button btn2 = (Button) findViewById(R.id.btn2);
        btn2.setOnClickListener(this);

        Button btn3 = (Button) findViewById(R.id.btn3);
        btn3.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btn2) {
            // Codi btn2
            Toast.makeText(getApplicationContext(), "Exemple click btn2", Toast.LENGTH_LONG).show();
        } else if (v.getId()== R.id.btn3) {
            // Codi btn3
            Toast.makeText(getApplicationContext(), "Exemple click btn3", Toast.LENGTH_LONG).show();
        }
    }

    public void btn4OnClick(View v) {
        //Codi btn4
        Toast.makeText(getApplicationContext(), "Exemple click btn4", Toast.LENGTH_LONG).show();
    }
}
